# custom-spring-boot-starter

#### <font color=blue>介绍</font>
基于Spring boot框架，自定义Starter

#### <font color=blue>模块介绍</font>

##### <font color = Orange>1. underline2Hump-spring-boot-web-starter</font>：入参下划线自动转驼峰，[点击查看详情](https://gitee.com/amvilcresx/custom-spring-boot-starter/tree/master/underline2Hump-spring-boot-web-starter).

##### <font color = Orange>2.  sms-spring-boot-starter</font>：整合多家短信服务商，实现配置即可使用，[点击查看详情](https://gitee.com/amvilcresx/custom-spring-boot-starter/tree/develop/sms-spring-boot-starter).

##### <font color = Orange>3. xml2bean-spring-boot-starter</font>：处理对象转XML时，需要对某些字段加上CDATA标签，[点击查看详情](https://gitee.com/amvilcresx/custom-spring-boot-starter/tree/develop/xml2bean-spring-boot-starter).

##### <font color = Orange>4. avcmail-spring-boot-starter</font>：通过配置邮件账号和授权码，即可实现发送简单文本邮件的功能，[点击查看详情](https://gitee.com/amvilcresx/custom-spring-boot-starter/tree/develop/avcmail-spring-boot-starter).