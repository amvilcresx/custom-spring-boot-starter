package pers.sms.springboot.annotation;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author amvilcresx
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({SmsAutoConfiguration.class})
public @interface EnableSms {
}
