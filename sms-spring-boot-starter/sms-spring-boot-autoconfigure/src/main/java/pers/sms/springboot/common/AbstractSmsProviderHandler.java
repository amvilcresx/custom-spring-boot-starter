package pers.sms.springboot.common;

import pers.sms.springboot.ali.AliSmsRequest;
import pers.sms.springboot.ali.AliSmsSenderHandler;
import pers.sms.springboot.exception.SmsException;

import java.util.Map;

/**
 * 短信服务商抽象类
 *
 * @author amvilcresx
 *
 * @see AliSmsSenderHandler
 */
public abstract class AbstractSmsProviderHandler implements SmsProviderHandler {


}
