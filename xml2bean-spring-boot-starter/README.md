>  该starter目的是为了解决Java对象转XML之后，针对某些属性的值需要加上`<![CDATA[ ]]>` 标签.

##### 0. 环境说明

* Spring boot版本：`2.1.0.RELEASE`
* JDK版本：1.8
* 运行的WEB环境：NONE、SERVLET

##### 1. 准备说明

* 下载starter的jar包，[点击下载](https://pan.baidu.com/s/1KMJ9BIT6MTsZdsW4HYGzeA)，提取码：`ivnx`。

* 将其放入Maven仓库：

  ```shell
  mvn install:install-file -Dfile=xml2bean-spring-boot-starter-1.0.0.RELEASE.jar -DgroupId=pers.avc.springboot -DartifactId=xml2bean-spring-boot-starter -Dversion=1.0.0.RELEASE -Dpackaging=jar
  ```

##### 2. 使用说明

* 引入maven依赖

  ```shell
   <dependency>
         <groupId>pers.avc.springboot</groupId>
         <artifactId>xml2bean-spring-boot-starter</artifactId>
         <version>1.0.0.RELEASE</version>
  </dependency>
  ```

* 对于要用`CDATA` 标签包裹的值，需要配合`@XStreamConverter` 注解使用，如下：

  > CDATAConvert 是自定义转换器，用于包裹目标值
  >
  > 注意：需要包裹的值得类型<u>**必须是String类型**</u>. 因为按照这里的处理方式,是将包裹后的值再重新赋值给原字段.

  ```java
  public class Teacher {
  
      /**
      *  <teacher>
      	<teacher_name><![CDATA[老师名字]]></teacher_name>
      	<age>12</age>
   	   </teacher>
      */
      @XStreamAlias("teacher_name")
      @XStreamConverter(CDATAConvert.class)
      private String teacherName;
  
      private Integer age;
  }
  ```

* 示例

  ```java
  @SpringBootApplication
  public class Xml2BeanBootstrap {
  
      public static void main(String[] args){
          SpringApplicationBuilder builder = new SpringApplicationBuilder(Xml2BeanBootstrap.class);
          ConfigurableApplicationContext context = builder.web(WebApplicationType.NONE).run(args);
  
          ClassRoom room = new ClassRoom();
          Student stu = new Student("张三", 1L+"", true);
          Student stu2 = new Student("李四", 2L+"", false);
          Teacher teacher = new Teacher("老师", 12);
          room.setClassName("三年A版");
          ArrayList<Student> stus = new ArrayList<>();
          stus.add(stu); stus.add(stu2);
          room.setStudents(stus);
          // room.setStudent(stu);
          room.setTeacher(teacher);
          // XmlUtils 是 jar包提供的工具类
          String xmlStr = XmlUtils.bean2Xml(room);
          System.out.println(xmlStr);
          room = XmlUtils.xml2Bean(xmlStr, new Class[]{ClassRoom.class, Student.class});
          System.out.println(room);
  
          context.close();
      }
  }
  ```

  结果：

  ![image-20210220111713131](https://i.loli.net/2021/02/20/xCEpUQHJdWZt2s6.png)

