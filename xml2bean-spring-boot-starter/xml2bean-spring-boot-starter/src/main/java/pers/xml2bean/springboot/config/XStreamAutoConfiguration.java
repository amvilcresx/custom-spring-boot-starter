package pers.xml2bean.springboot.config;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import pers.xml2bean.springboot.core.XStreamFactory;
import pers.xml2bean.springboot.core.XmlUtils;

/**
 * @description XStream 相关Bean自动装配
 * @author AmVilCresx
 */
@EnableConfigurationProperties(XStreamConfigProperties.class)
public class XStreamAutoConfiguration {

    @Bean
    public XStream xStream(XStreamConfigProperties config) {
        return XStreamFactory.buildXStream(new XmlFriendlyNameCoder("_-", "_"), config.getIgnores());
    }

    @Bean
    public XmlUtils xmlUtils() {
        return new XmlUtils();
    }
}
