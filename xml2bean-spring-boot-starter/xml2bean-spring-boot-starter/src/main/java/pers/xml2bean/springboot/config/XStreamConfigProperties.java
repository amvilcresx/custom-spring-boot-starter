package pers.xml2bean.springboot.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @description XStream 属性配置类
 * @author amvilcresx
 */
@ConfigurationProperties(prefix = "xstream")
public class XStreamConfigProperties {

    List<String> ignores;

    public List<String> getIgnores() {
        return ignores;
    }

    public void setIgnores(List<String> ignores) {
        this.ignores = ignores;
    }
}
