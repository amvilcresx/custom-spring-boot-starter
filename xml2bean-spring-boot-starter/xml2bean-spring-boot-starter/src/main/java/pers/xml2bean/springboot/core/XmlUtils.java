package pers.xml2bean.springboot.core;

import com.thoughtworks.xstream.XStream;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * @description XML 工具类
 * @author AmVilCresx
 */
public class XmlUtils implements ApplicationContextAware {

    private static XStream xStream;

    public static XStream getXStream() {
        return xStream;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        XmlUtils.xStream = applicationContext.getBean(XStream.class);
    }

    public static String bean2Xml(Object obj){
        return xStream.toXML(obj);
    }

    public static <T> T xml2Bean(String xmlStr, Class<?>[] clzArr) {
        xStream.processAnnotations(clzArr);
        return  (T) xStream.fromXML(xmlStr);
    }

    public static <T> T xml2Bean(String xmlStr, Class<?> clz) {
        xStream.processAnnotations(clz);
        return  (T) xStream.fromXML(xmlStr);
    }

    /**
     * 将 xml 字符串转Map
     * @param xmlStr xml字符串
     * @return Map
     * @throws Exception 转换异常
     */
    public static Map<String, Object> xml2Map(String xmlStr) throws Exception{
        Map<String, Object> map = new HashMap<>();
        if (!StringUtils.hasText(xmlStr)) {
            throw new IllegalArgumentException("xml字符串不可为空");
        }

        Document document = DocumentHelper.parseText(xmlStr);
        Element root = document.getRootElement();
        xml2Map(root, map);
        return map;
    }

    /**
     * xml转map
     *
     * @param rootElem 跟元素
     * @param map map
     */
    public static void xml2Map(Element rootElem, Map<String, Object> map) {
        if (Objects.isNull(rootElem) || Objects.isNull(map)) {
            return;
        }
        List<Element> subElements = rootElem.elements();
        if (subElements == null || subElements.size() <= 0){
            return;
        }
        for (Element subElement : subElements){
            String name = subElement.getName();
            Object data;
            if (subElement.isTextOnly()){
                data = subElement.getData();
            }else{
                data = new HashMap<String, Object>();
                xml2Map(subElement, (Map<String, Object>)data);
            }
            Object preData = map.remove(name);
            if (preData != null){
                if (preData instanceof List){
                    ((List) preData).add(data);
                    map.put(name, preData);
                }else{
                    List list = new ArrayList();
                    list.add(preData);
                    list.add(data);
                    map.put(name, list);
                }
            }else if (data != null){
                map.put(name, data);
            }
        }
    }

}
