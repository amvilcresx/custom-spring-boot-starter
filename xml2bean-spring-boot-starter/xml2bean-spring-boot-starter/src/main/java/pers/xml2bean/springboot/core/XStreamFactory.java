package pers.xml2bean.springboot.core;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.thoughtworks.xstream.io.xml.XppDomDriver;
import org.springframework.util.CollectionUtils;

import java.io.Writer;
import java.util.List;

/**
 * @description XStream 对象工厂类
 * @author amvilcresx
 */
public class XStreamFactory {

    private volatile static XStream xStream;

    public static XStream buildXStream(XmlFriendlyNameCoder nameCoder, List<String> ignores) {

        if (xStream == null) {
            synchronized (XStreamFactory.class){
               if (xStream == null) {
                   xStream = new XStream(new XppDomDriver(nameCoder) {
                       @Override
                       public HierarchicalStreamWriter createWriter(Writer out) {
                           return new PrettyPrintWriter(out, nameCoder) {
                               @Override
                               public void startNode(String name, Class clazz) {
                                   super.startNode(name, clazz);
                               }
                               @Override
                               public void writeText(QuickWriter writer, String text) {
                                   writer.write(text);
                               }
                           };
                       }
                   });
                   xStream.autodetectAnnotations(true);
                   // 去掉 不需要的标签属性,如 ："class", "reference"
                   if (!CollectionUtils.isEmpty(ignores)) {
                       ignores.forEach(item -> xStream.aliasSystemAttribute(null, item));
                   }
               }
            }
        }

        return xStream;
    }
}
