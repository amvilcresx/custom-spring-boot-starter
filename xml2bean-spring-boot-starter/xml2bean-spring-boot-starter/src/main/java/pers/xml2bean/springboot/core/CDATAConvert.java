package pers.xml2bean.springboot.core;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import org.apache.commons.lang.StringEscapeUtils;

/**
 * @description 为xml的字段加上CDATA标签
 * @author AmVilCresx
 */
public class CDATAConvert implements Converter {

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        String prefix = "<![CDATA[";
        String suffix = "]]>";
        String trans = prefix + source + suffix;
        writer.setValue(StringEscapeUtils.unescapeXml(trans));
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        return reader.getValue();
    }

    @Override
    public boolean canConvert(Class type) {
        return String.class.isAssignableFrom(type);
    }
}
