package person.springboot.xml2bean;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import pers.xml2bean.springboot.core.XmlUtils;
import person.springboot.xml2bean.entity.ClassRoom;
import person.springboot.xml2bean.entity.Student;
import person.springboot.xml2bean.entity.Teacher;

import java.util.ArrayList;
import java.util.Map;


/**
 * Xml2Bean 测试类
 *
 * @author amvilcresx
 */
@SpringBootApplication
public class Xml2BeanBootstrap {

    public static void main(String[] args) throws Exception{
        SpringApplicationBuilder builder = new SpringApplicationBuilder(Xml2BeanBootstrap.class);
        ConfigurableApplicationContext context = builder.web(WebApplicationType.NONE).run(args);

        ClassRoom room = new ClassRoom();
        Student stu = new Student("张三", 1L+"", true);
        Student stu2 = new Student("李四", 2L+"", false);
        Teacher teacher = new Teacher("老师", 12);
        room.setClassName("三年A版");
        ArrayList<Student> stus = new ArrayList<>();
        stus.add(stu); stus.add(stu2);
        room.setStudents(stus);
        // room.setStudent(stu);
        room.setTeacher(teacher);
        System.out.println("对象转XML：======================");
        String xmlStr = XmlUtils.bean2Xml(room);
        System.out.println(xmlStr);
        room = XmlUtils.xml2Bean(xmlStr, new Class[]{ClassRoom.class, Student.class});
        System.err.println("XML转对象：======================");
        System.err.println(room);
        System.err.println("Xml 转Map：》》》》》");
        String str = "<xml>\n" +
                "  <ToUserName><![CDATA[toUser]]></ToUserName>\n" +
                "  <FromUserName><![CDATA[fromUser]]></FromUserName>\n" +
                "  <CreateTime>12345678</CreateTime>\n" +
                "  <MsgType><![CDATA[image]]></MsgType>\n" +
                "  <Image>\n" +
                "    <MediaId><![CDATA[media_id]]></MediaId>\n" +
                "  </Image>\n" +
                "</xml>";
        Map<String,Object> map = XmlUtils.xml2Map(str);
        map.forEach((k,v) -> {
            System.out.println(k + "____" + v);
        });

        context.close();
    }
}
