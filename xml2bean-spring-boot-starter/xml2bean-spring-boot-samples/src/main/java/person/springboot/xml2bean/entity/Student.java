package person.springboot.xml2bean.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import pers.xml2bean.springboot.core.CDATAConvert;

/**
 * @description Student 对象
 * @author AmVilCresx
 */
@XStreamAlias("student")
public class Student {

    private String name;

   // @XStreamCDATA
    @XStreamAlias("_id")
    @XStreamConverter(CDATAConvert.class)
    private String id;

    private Boolean gender;

    public Student(String name, String id, Boolean gender) {
        this.name = name;
        this.id = id;
        this.gender = gender;
    }

    public Student() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", gender=" + gender +
                '}';
    }
}
