package person.springboot.xml2bean.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import pers.xml2bean.springboot.core.CDATAConvert;

/**
 * @description Teacher 对象
 * @author AmVIlCresx
 */
public class Teacher {

    @XStreamAlias("teacher_name")
    @XStreamConverter(CDATAConvert.class)
    private String teacherName;

    private Integer age;

    public Teacher(String teacherName, Integer age) {
        this.teacherName = teacherName;
        this.age = age;
    }

    public Teacher() {
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "teacherName='" + teacherName + '\'' +
                ", age=" + age +
                '}';
    }
}
