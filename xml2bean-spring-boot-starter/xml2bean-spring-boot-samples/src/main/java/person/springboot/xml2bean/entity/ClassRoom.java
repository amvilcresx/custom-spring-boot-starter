package person.springboot.xml2bean.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import org.apache.commons.lang.ArrayUtils;
import pers.xml2bean.springboot.core.CDATAConvert;

import java.util.Arrays;
import java.util.List;

/**
 * @description ClassRoom 对象
 * @author AmVilCresx
 */

@XStreamAlias("classRoom")
//@XStreamCDATA
public class ClassRoom {

    @XStreamAlias("class_name")
    //@XStreamCDATA
    @XStreamConverter(CDATAConvert.class)
    private String className;

    //@XStreamAlias("single_stu")
    // @XStreamCDATA
    //@XStreamSubAttr
    private Teacher teacher;


    @XStreamAlias("students")
    //@XStreamCDATA
    //@XStreamSubAttr(type = XStreamSubAttr.SubAttrType.GENERIC)
    private List<Student> students;

//    private Student student;
//
//    public Student getStudent() {
//        return student;
//    }
//
//    public void setStudent(Student student) {
//        this.student = student;
//    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @Override
    public String toString() {
        return "ClassRoom{" +
                "className='" + className + '\'' +
                ", teacher=" + teacher +
                ", students=" + Arrays.deepToString(students.toArray()) +
                '}';
    }
}
