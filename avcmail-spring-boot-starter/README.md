##### 0. 说明

* 开发时Spring boot版本：`2.1.0.RELEASE`
* JDK版本：1.8+
* 运行的WEB环境：NONE、SERVLET

##### 使用说明

* 在spring boot配置文件中添加必要的配置

  ```yaml
  avc:
    mail:
      host: smtp.qq.com
      # 发送方账号
      username: 22xxxx19@qq.com
      # 邮箱授权码，不是密码
      password: cfdkxxxxkdjhg
      properties:
        mail:
          transportprotocol: smtp
          smtp:
            host: smtp.qq.com
            ssl:
              enable: true
            port: 465
            starttls:
              enable: true
  ```

* 在启动类上配置`@EnableAvcMail` 注解。

* 示例代码

  ```java
  @EnableAvcMail
  public class AvcMailBootstrap {
      public static void main(String[] args) throws MessagingException {
          ConfigurableApplicationContext context = new SpringApplicationBuilder(AvcMailBootstrap.class)
                  .web(WebApplicationType.NONE)
                  .run(args);
          AvcMailSender sender = context.getBean(AvcMailSender.class);
        // sender.send(new String[]{"15479ss612@qq.com"}, "这是主题", "测试内容");
          // sender.send("15479ss612@qq.com", "这是主题", "测试内容");
          System.out.println("获取Sender的Bean实例：" + sender);
          context.close();
      }
  }
  ```
  