package pers.avcmail.springboot.configure;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *  邮件发送核心类
 * @date 2020/8/25
 * @author AmVilCresx
 */

public class AvcMailSender {

    private final Properties properties;

    private final String host;

    private final String user;

    private final String password;

    private final String protocol;

    public AvcMailSender(String protocol, String host, String user, String password, Map<String, String> source) {
        this.protocol = protocol;
        this.host = host;
        this.user = user;
        this.password = password;
        properties = new Properties();
        properties.putAll(source);
    }

    public void send(String to, String subject, String content) throws MessagingException{
        send(new String[]{to}, subject, content);
    }

    public void send(String[] to, String subject, String content) throws MessagingException {
        Session session = Session.getInstance(properties);
        /// session.setDebug(true); // 是否在控制台打出语句
        // 定义从哪个邮箱到哪个邮箱的地址和内容
        MimeMessage message = new MimeMessage(session);

        message.setFrom(new InternetAddress(user));
        message.addRecipients(Message.RecipientType.TO, buildToNetAddress(to));
        message.setSubject(subject);
        message.setText(content);//邮件的正文内容
        message.saveChanges();
        Transport tran = session.getTransport(protocol); // 通过SMTP效验用户，密码等进行连接
        tran.connect(host, user, password);
        tran.sendMessage(message, message.getAllRecipients());
        tran.close();
    }

    private InternetAddress[] buildToNetAddress(String[] to) throws AddressException {
        List<InternetAddress> netAddrs = new ArrayList<>(to.length);
        for (String addr : to) {
            netAddrs.add(new InternetAddress(addr));
        }
        return netAddrs.toArray(new InternetAddress[0]);
    }
}
