package pers.avcmail.springboot.configure.annotation;

import org.springframework.context.annotation.Import;
import pers.avcmail.springboot.configure.AvcMailAutoConfiguration;

import java.lang.annotation.*;

/**
 *  Enable 激活注解
 * @date 2020/8/28
 * @author AmVilCresx
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(AvcMailAutoConfiguration.class)
public @interface EnableAvcMail {

}
