package pers.avcmail.springboot.configure;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 *  自动装配
 * @date 2020/8/25
 * @author AmVilCresx
 */

@Configuration
@Import({AvcMailSenderPropertiesConfiguration.class})
public class AvcMailAutoConfiguration {

}
