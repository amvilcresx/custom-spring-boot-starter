package pers.avcmail.springboot.configure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

/**
 *  发送邮件所需的配置信息
 * @date 2020/8/25
 * @author AmVilCresx
 */

@ConfigurationProperties(prefix = "avc.mail")
@Data
public class AvcMailProperties {

    private String host;

    private String username;

    private String password;

    private String protocol = "smtp";

    private Map<String, String> properties = new HashMap<>();
}
