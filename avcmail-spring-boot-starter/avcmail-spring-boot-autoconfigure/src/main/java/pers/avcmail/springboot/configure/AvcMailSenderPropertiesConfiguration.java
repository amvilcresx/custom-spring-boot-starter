package pers.avcmail.springboot.configure;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *  描述类的功能
 * @date 2020/8/27
 * @author 焦召军
 */
@Configuration
@EnableConfigurationProperties(AvcMailProperties.class)
public class AvcMailSenderPropertiesConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public AvcMailSender avcMailSender(AvcMailProperties avcMailProperties){
        return new AvcMailSender(avcMailProperties.getProtocol(),
                avcMailProperties.getHost(),
                avcMailProperties.getUsername(),
                avcMailProperties.getPassword(),
                avcMailProperties.getProperties());
    }
}
