package perms.avcmail.springboot.sample;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import pers.avcmail.springboot.configure.AvcMailSender;
import pers.avcmail.springboot.configure.annotation.EnableAvcMail;

import javax.mail.MessagingException;

/**
 *  自定义邮件发送starter测试引导类
 * @date 2020/8/28
 * @author 焦召军
 */

@SpringBootApplication
@EnableAvcMail
public class AvcMailBootstrap {

    public static void main(String[] args) throws MessagingException {
        ConfigurableApplicationContext context = new SpringApplicationBuilder(AvcMailBootstrap.class)
                .web(WebApplicationType.SERVLET)
                .run(args);
        AvcMailSender sender = context.getBean(AvcMailSender.class);
        //sender.send(new String[]{"1547959612@qq.com"}, "这是主题", "测试内容");
        System.out.println("获取Sender的Bean实例：" + sender);
        //context.close();
    }
}

