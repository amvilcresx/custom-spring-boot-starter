package pers.under2hump.springboot.configure.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import pers.under2hump.springboot.configure.core.AvcHandlerMethodArgumentResolver;
import pers.under2hump.springboot.configure.core.AvcParameterFilter;
import pers.under2hump.springboot.configure.core.Underline2HumpConstant;

/**
 * @description 自动装配类
 * @author AmVilCresx
 */
@ComponentScan(Underline2HumpConstant.SCAN_COMPONENT_PATH)
public class UnderLineToHumpAutoConfiguration {

    @Bean
    @ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
    public AvcParameterFilter parameterFilter(){
        return new AvcParameterFilter();
    }

    @Bean
    public AvcHandlerMethodArgumentResolver avcHandlerMethodArgumentResolver () {
        return new AvcHandlerMethodArgumentResolver();
    }
}
