package pers.under2hump.springboot.configure.core;

/**
 * @description 常量类
 * @author AmVilCresx
 */
public interface Underline2HumpConstant {

    /**
     * 包扫描路径
     */
    String SCAN_COMPONENT_PATH = "pers.under2hump.springboot";
}
